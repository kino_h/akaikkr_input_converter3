#!/usr/bin/env python

import sys
import os
import NME.AkaiKKR
import argparse

def process_arg():
    parser = argparse.ArgumentParser(description='AkaiKKR converter')
    parser.add_argument('-o', '--outputfilename', dest='outputfilename',
        type=str, help='AkaiKKR filename (is an output of this tool)', default="kkrinput", required=False) 
    parser.add_argument('inputfilename',  type=str, # nargs='+',
                    help='input cif filename' )
    args = parser.parse_args()
    return args


if __name__ == '__main__':

    args = process_arg() 
    filename = args.inputfilename
    kkrinputfile = args.outputfilename

    headname,tailname = os.path.split(filename)
    datafilename = tailname.split(".")[0]
    filetype = tailname.split(".")[1] 

    #   coordtype="frac" or "cart"
    coordtype="frac"
    #coordtype="cart"
    param = { "go":"j", "edelt": 0.005 } 
    inputparser = NME.AkaiKKR.InputParser( datafilename, coordtype=coordtype, param=param )

    if filetype=="cif":
        inputparser.from_ciffile(filename)

    elif filetype=="litecif":
        inputparser.from_liteciffile(filename)

        print "NME.AkaiKKR.InputParser is called with coordtype=",coordtype
    #  You can change kkr parameter here. 

    #kkrinputfile = "kkrinput"
    inputparser.write_file(kkrinputfile)
    print kkrinputfile,"is made."
