# -*- coding: utf-8 -*-
import sys
import subprocess
import json
import os

#import NME.liteCif 
import liteCif 

class tspaceCaller:
    """ tspaceとの連携を行う
    """
    def __init__(self,dic):
        """ input dic

            1.programの確認を行う。WRITE1WYもしくは、このファイルのあるdirectoryから実行ファイルpathを確認する。
            2. dic -> self.alen, などに変換する。
        """
        try:
          self.prog = os.environ["WRITE1WY"]
        except: 
          dirname = os.path.dirname(__file__)
          self.prog = os.path.join(dirname,"write1wy") 
        self.dic = dic 
        self.id_ = dic["_symmetry_Int_Tables_number"] 
        self.alen = dic["_cell_length_a"]
        self.blen = dic["_cell_length_b"]
        self.clen = dic["_cell_length_c"]
        self.alpha = dic["_cell_angle_alpha"]
        self.beta = dic["_cell_angle_beta"]
        self.gamma = dic["_cell_angle_gamma"]
        self.wys_list = []
        self.set_spacegroup(self.id_)

    def show(self):
        print "self.wys_list" 
        print self.wys_list 

    def set_spacegroup(self,id_):
        """ input: id_ = spacegroup ID
           output: choiceが複数ある場合も wyckoff position listを返す。
                   origin choiceが２つある場合は wys_list[0], wys_list[1]が入る。
        """
        self.id_ = id_
        self.get_nchoice()
        self.wys_list = []
        print "origin choice =", self.dic["choice"] 
        for  i in range(self.dic["choice"]) :
            self.wys_list.append( self.get_wy(i+1, 
               [self.alen, self.blen, self.clen, 
               self.alpha, self.beta, self.gamma]) ) 
        
    def get_nchoice(self):
        """   space group ID = self.id_ から origin choiceの数を得る
        """
        cmd = [ self.prog,  "query", str(self.id_) ]
        with open("__stdout_choice.stdout", "w") as fout:
          with open("__stdout_choice.stderr", "w") as ferr:
            subprocess.call ( cmd , stdout = fout, stderr = ferr) 
        with open("choice.json","r") as f:
           dic = json.load(f)
           self.dic.update(dic)

    def get_wy(self,ichoice,abcangle):
        """   self.id_とichoice, abcangleから
              1. 文字でのwyckoff positionの情報を得る
              2. 格子パラメタを得る。

            失敗したらExceptionを返す
        """
        cmd = [ self.prog,  "wy", str(self.id_),str(ichoice) ]
        abcangle = map(str,abcangle) 
        cmd.extend(abcangle)
        with open("__stdout_wy.stdout","w") as fout:
          with open("__stdout_wy.stderr","w") as ferr:
            subprocess.call ( cmd, stdout = fout, stderr = ferr )
        dic = {}
        with open("wy.json","r") as f:
           dic = json.load(f)
        with open("LATreal.json","r") as f:
           dic.update( json.load(f) )
           return dic
        print "failed to run the program"
        print cmd

        raise Exception("failed to run the program")


class Generator:
    def __init__(self,litecif):
        """ input: litecif class data
            tspaceを用いて, 格子、wyckoff positionの全情報を得る。
        """
        self.cifinput = litecif 
        print "generator, litecif=", litecif 
        #self.tsp = tspaceCaller( litecif.dic["_symmetry_Int_Tables_number"] )
        self.tsp = tspaceCaller( litecif.dic )
        self.validate_position()
        self.make_all_equiv()

    @staticmethod
    def match_position(positions,wys):
        """ originの判定を自動的に行う.
        """
        def get_wyc(wys,ch):
            """ input
                wys = wyckoff position list 
                ch = wyckoff character
                output  
                chに対応するwyckoff positionを返す

                得られない場合はExceptionを返す
            """
                
            for wy in wys:
               if wy["wyc"] == ch : 
                   return wy
            print "failed to find wyc"
            print "ch=",ch
            print [ w["wyc"] for w in wys ] 
            print "wys=", wys
            raise Exception("failed to find wyc")

        def process_eq(ch,a,p):
            """  
                  input:
                   ch= string, a=float, p=float,  listではない。

                  return 
                     dic[ch]= float 

                   ch + a = p という形式で与えられる場合にxを返す
                  例：
                  "2x" + 0.5 = 0.6 


                   失敗した場合Exceptionを返す

            """ 
 
            newdic = {} 
            x =  p - a 
            symbols = [ "-2x",  "-z", "-y", "-x", "0",  "x", "y", "z",  "2x" ]
            factor = [ ["x", -0.5 ] , ["z", -1.0], ["y", -1.0], ["x", -1.0 ], ["0",1.0], ["x", 1.0], ["y", 1.0], ["z", 1.0], ["x", 0.5] ]
            if len(symbols) != len(factor):
               print "proces_eq", "internal error, len(symbols) != len(factor)"
               raise Exception("internal error")
            if ch in symbols:
               i = symbols.index(ch)
               s = factor[i][0]
               f = factor[i][1]
               newdic[s] = x * f
               return newdic 
            else:
               print __name__, "unknown symbol,  ch=", ch 
               raise Exception("unknown symbol")


        def process_equations( valuedic, xyzch , add, pos ):
            """
                 input: xyzch, add, pos 
                         3-element list 

                   xyzch + add = pos となる

                 output valuedic

                      valuedic["x"] = 0.1
                      valuedic["y"] = 0.15
                      の形式で帰ってくる

                wyckoff positionごとにこれを行う。
 
                失敗した場合にExceptionを返す
            """

                
            eps = 1.0e-6 
            print "process_equations", xyzch, add, pos 
            for ch, a, p in zip(xyzch, add,pos ):
               newvalue =  process_eq( ch,a,p ) 
               key = newvalue.keys()[0] 
               if key in valuedic : 
                   if abs ( newvalue[key] - valuedic[key] ) > eps: 
                     print "value is inconsistent"
                     print ch,a,p 
                     print xyzch,add,pos 
                     raise Exception("inconsistent value") 
               else:
                   valuedic.update(  newvalue  )
            x = 0.0
            try:
               x = valuedic["0"]
            except:
               pass 
            if abs(x) > eps :
                   raise Exception("inconsistent value")
            return valuedic 


        """
             wyckoff  postionごとに処理を行う。

             data 構造がわかりにくい・・・。

        """
        wyvarlist = []
        for atom in  positions.atoms:
            valuedic = {}
            wy_char = atom["_atom_site_Wyckoff_symbol"] 
            pos = map( float, [ atom["_atom_site_fract_x"], atom["_atom_site_fract_y"], atom["_atom_site_fract_z"] ] )
            print "wychar=",wy_char, pos
           
            wy = get_wyc( wys["wy"] , wy_char )
            try: 
                valuedic.update( process_equations( valuedic, wy["xyzch"] , wy["add"], pos ) )
            except Exception:
                raise Exception("inconsistent value")   
            wyvarlist.append(valuedic)

        return wyvarlist 

        
    def validate_position(self):
        """ origin choiceはinputにない。
            origin choiceごとにwyckoff positionを比較して、どのorigin choiceかを選択する。

            output: 
               iorigin
               wyvarlist : 選択されたoriginでのwyckoff positions
               primitivevector 
               conventionalvector  
        """
        #print "choice=",self.tsp.dic["choice"]
        #if self.tsp.dic["choice"]==1:
        #    self.choice = 0
        #    return 

        # loop of origin_choice
        for iorigin,wys in enumerate(self.tsp.wys_list) : 
            try: 
                print "try, origin =", iorigin 
                print "wy="
                for x in wys["wy"]:
                    print x

                wyvarlist = self.match_position(self.cifinput, wys )
            except Exception:
                print "failed origin=", iorigin
                continue
            print "-----------------"
        self.iorigin = iorigin
        self.wyvarlist = wyvarlist 
        self.primitivevector = self.tsp.wys_list[iorigin]["primitivevector"]
        self.conventionalvector = self.tsp.wys_list[iorigin]["conventionalvector"]

    def make_all_equiv(self):
        """ Wyckoff position 代表点から等価なpositionを作成する。
        """

        def apply_var(var,wys):
            """ 2x, x-yなどで与えられる値を数値に変換する。
                input 
                  var["x"]などdic
                  wys["xyzch"]
                  wys["add"]

                 wys["xyzch"] + wys["add"] の計算
            """
            #symbols = [ "-2x", "-x+y", "-z", "-y", "-x", "0",  "x", "y", "z", "x-y", "2x" ]
            vlist =  []
            if True:
                xyzch = wys["xyzch"]
                add = wys["add"]
                v3 = []
                for  c, a in zip(xyzch,add):
                     v = a
                     if c =="-2x":
                          v += -2.0*var["x"] 
                     elif c=="-x+y":
                          v += -var["x"] + var["y"]
                     elif c=="-z":
                          v += -var["z"] 
                     elif c=="-y":
                          v += -var["y"] 
                     elif c=="-x":
                          v += -var["x"] 
                     elif c=="0":
                          v += 0.0
                     elif c=="x":
                          v += var["x"] 
                     elif c=="y":
                          v += var["y"] 
                     elif c=="z":
                          v += var["z"]
                     elif c=="x-y":
                          v += var["x"] - var["y"]
                     elif c =="2x":
                          v += 2.0*var["x"]
                     else:
                          print "key word error in value"
                          print xyzch
                          print wys
                          raise Exception("key word error in value")
                     v3.append(v)
                vlist = v3
            return vlist 


        def make_equiv(wych, var, wys_list ):
            """ 等価なwyckoff position listの作成
            """
            found = False
            mul = ""
            for iwys,wy in enumerate(wys_list["wy"]):
                if wy["wyc"] == wych:
                    mul = wy["mul"] 
                    found = True
                    break
            if not found:
                print "failed to find wych=",wych
                print wyslist
                raise Exception("internal error")
            xyzlist = []
            for wyq in wys_list["wyq"][iwys]:
                xyz = apply_var(var, wyq )
                xyzlist.append(xyz)
            return xyzlist , mul 


        """ 代表点から等価なwyckoff positionを全部構成する。
            output: self.atoms 
        """

        allpos = []
        for pos,var in zip(self.cifinput.atoms, self.wyvarlist):
            #frac = [ pos["_atom_site_fract_x"], pos["_atom_site_fract_y"], pos["_atom_site_fract_z"]  ]
            wych = pos["_atom_site_Wyckoff_symbol"] 
            label = pos["_atom_site_label"] 
            equiv_pos,mul =  make_equiv(wych, var, self.tsp.wys_list[ self.iorigin ] )
            equiv_atom = []
            for eqp in equiv_pos:
                equiv_atom.append( [ label, mul, wych, eqp ] )
            allpos.append(equiv_atom)

        self.atoms = allpos 
            
            

if __name__ == "__main__":

# export WRITE1WY=/home/kino/work/tspace/spacegroup.tspaceB4/write1wy
    #filename =  "4295227633-1-2.ciflite"
    #filename =  "4295500997-1-2.ciflite"
    filename = sys.argv[1]

    litecif = liteCif.Reader(filename)
    print "cif=",litecif
    gen = Generator(litecif)
    
    print gen.primitivevector
    print gen.conventionalvector
    for ipos,pos in enumerate(gen.atoms):
        print "group",ipos
        for p in pos:
            print p 


